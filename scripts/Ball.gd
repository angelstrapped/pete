extends RigidBody

const DEBUG = false

export(Resource) var debug_dot

onready var stream_player = get_node("AudioStreamPlayer3D")

func _on_body_entered(_body):
	stream_player.play()

func _integrate_forces(state):
	if DEBUG:
		var deb = debug_dot.instance()
		deb.global_transform = global_transform
		get_tree().root.add_child(deb)

	if state.get_contact_count() > 0:
		stream_player.unit_db = abs(state.get_contact_impulse(0)) * 0.15
		
		if DEBUG:
			var deb2 = debug_dot.instance()
			deb2.global_transform.origin = to_global(state.get_contact_local_position(0))
			deb2.mesh = deb2.mesh.duplicate()
			deb2.mesh.set("radius", 0.1)
			deb2.mesh.set("height", 0.2)
			get_tree().root.add_child(deb2)
