extends Spatial

const DEBUG = false

onready var reel_one = get_node("Reel1")

var score: int = 0

func reset_score():
	score = 0
	reel_one.reset()

func add_score(number: int):
	score += number
	reel_one.add(number)

func _process(_delta):
	if DEBUG:
		if Input.is_action_just_pressed("ui_accept"):
			print("duh")
			reel_one.add(9)
		if Input.is_action_just_pressed("ui_down"):
			print("reset")
			reel_one.reset()
