extends Area


export(Resource) var fake_ball_scene


var pending_balls_count: int = 100

func add_balls(number: int):
	pending_balls_count += number
	
func _physics_process(_delta):
	if pending_balls_count == 0:
		return

	if len(get_overlapping_bodies()) == 0:
		_spawn_fake_ball()

func _spawn_fake_ball():
	if pending_balls_count > 0:
		pending_balls_count -= 1
		var new_fake_ball = fake_ball_scene.instance()
		add_child(new_fake_ball)
