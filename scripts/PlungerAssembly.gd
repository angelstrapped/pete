extends Spatial


export(NodePath) var ball_tray_path


func _ready():
	get_node("BallFeed").ball_tray = get_node(ball_tray_path)
