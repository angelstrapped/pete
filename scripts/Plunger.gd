extends RigidBody


func _physics_process(_delta):
	# Push down on the plunger.
	add_central_force(-global_transform.basis.y * (Input.get_action_strength("plunger") * 96000.0))
