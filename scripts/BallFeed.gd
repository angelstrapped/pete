extends Area


onready var stream_player = get_node("AudioStreamPlayer3D")
onready var plunger = get_parent().get_node("Plunger/Plunger")

var ball_tray

var ball_dispensed_count: int = 0


func _on_body_exited(body):
	# Plunger pulled back.
	if body == plunger:
#		_spawn_free_ball()
		_spawn_ball()

func _spawn_ball():
	# Attempt to get a new ball from the ball tray.
	var new_ball
	if ball_tray:
		new_ball = ball_tray.get_ball()

	if new_ball == null:
		return

	# Add the ball to the scene.
	get_parent().add_child(new_ball)

	# Move the ball to our position.
	new_ball.global_transform = global_transform

	# Play a sound.
	stream_player.play()

	ball_dispensed_count += 1
	print(ball_dispensed_count)

func _spawn_free_ball():
	# Create a new ball.
	var new_ball = ball_tray.get_free_ball()

	# Move the ball to our position.
	new_ball.transform = transform

	# Add the ball to the scene.
	get_parent().add_child(new_ball)

	# Play a sound.
	stream_player.play()

	ball_dispensed_count += 1
	print(ball_dispensed_count)
