extends Spatial


export(NodePath) var score_reels_path
export(NodePath) var fake_spawner_path
export(NodePath) var payout_path

onready var score_reels = get_node(score_reels_path)
onready var fake_spawner = get_node(fake_spawner_path)
onready var payout = get_node(payout_path)


func _input(event):
	if event.is_action_released("ante"):
		_ante()
	
	elif event.is_action_released("payout"):
		_payout()

# Pay out scored balls to the play tray.
func _ante():
	var current_score = score_reels.score
	score_reels.reset_score()
	fake_spawner.add_balls(current_score)

# Pay out scored balls to the redemption tray.
func _payout():
	var current_score = score_reels.score
	score_reels.reset_score()
	payout.payout(current_score)
