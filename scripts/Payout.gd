extends Spatial


export(Resource) var real_ball_scene
var pending_payouts: int = 0

func _physics_process(_delta):
	if pending_payouts > 0:
		var new_ball = real_ball_scene.instance()
		new_ball.transform.origin = transform.origin
		new_ball.set_axis_lock(
			PhysicsServer.BODY_AXIS_ANGULAR_X |
			PhysicsServer.BODY_AXIS_ANGULAR_Y |
			PhysicsServer.BODY_AXIS_ANGULAR_Z |
			PhysicsServer.BODY_AXIS_LINEAR_X |
			PhysicsServer.BODY_AXIS_LINEAR_Y |
			PhysicsServer.BODY_AXIS_LINEAR_Z,
			false
			)
		get_parent().add_child(new_ball)
		pending_payouts -= 1

func payout(number: int):
	pending_payouts += number
