extends Spatial


signal ball_eaten


func _on_ball_eaten():
	emit_signal("ball_eaten")
