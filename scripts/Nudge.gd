extends Spatial

export(NodePath) var main_camera_path

const NUDGE_IMPULSE = Vector3(-100.0, 0.0, 0.0)

onready var camera_nudge = -NUDGE_IMPULSE * 0.001

onready var stream_player = get_node("AudioStreamPlayer3D")

onready var main_camera = get_node(main_camera_path)

func _physics_process(_delta):
	if Input.is_action_just_pressed("nudge"):
		for node in get_tree().get_nodes_in_group("nudge"):
			if node.is_class("RigidBody"):
				node.apply_central_impulse(NUDGE_IMPULSE)
		stream_player.play()
		main_camera.nudge(camera_nudge)
