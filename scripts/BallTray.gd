extends Area


export(Resource) var real_ball_scene


func get_ball():
	# Loop through stuff that is touching us and return the first ball.
	var balls = get_overlapping_bodies()
	for body in balls:
		if body.is_in_group("ball"):
			body.queue_free()
			var new_ball = real_ball_scene.instance()
			return new_ball

func get_free_ball():
	# Generate a "free" ball from out of nowhere, ignoring what's in the tray.
	var new_ball = real_ball_scene.instance()
	return new_ball
