extends Spatial


export(NodePath) var gravity_slider_path
export(NodePath) var gravity_text_path

onready var gravity_slider = get_node(gravity_slider_path)
onready var gravity_text = get_node(gravity_text_path)


func _ready():
	gravity_slider.value = PhysicsServer.area_get_param(
		get_viewport().find_world().get_space(),
		PhysicsServer.AREA_PARAM_GRAVITY
	)
	gravity_text.text = str(gravity_slider.value)

func _on_gravity_slider_value_changed(value):
	#print(value)
	gravity_text.text = str(int(value))

	# Change gravity (you need to do it this way for it to change at runtime.)
	PhysicsServer.area_set_param(
		get_viewport().find_world().get_space(),
		PhysicsServer.AREA_PARAM_GRAVITY,
		value
	)
	
	print(PhysicsServer.area_get_param(
		get_viewport().find_world().get_space(),
		PhysicsServer.AREA_PARAM_GRAVITY
	))
