extends ColorRect


var timer = 3.0
var alpha_factor = timer / 3.0


func _process(delta):
	timer -= delta
	color -= Color(0, 0, 0, delta / alpha_factor)
	
	if timer < 0.0:
		queue_free()
