extends Spatial


export(NodePath) var score_reel_path
export(NodePath) var speaker_path

var speaker
var score_reel

var stream_player_channel
var stream_player_who

var stream_player_hammer_one
var stream_player_hammer_two
var stream_player_hammer_three

var stream_player_nothing

func _score(number: int):
	if score_reel:
		score_reel.add_score(number)

func _ready():
	score_reel = get_node(score_reel_path)
	
	speaker = get_node(speaker_path)
	stream_player_channel = get_node("Channel")
	stream_player_who = get_node("Who")
	stream_player_hammer_one = get_node("HammerOne")
	stream_player_hammer_two = get_node("HammerTwo")
	stream_player_hammer_three = get_node("HammerThree")
	stream_player_nothing = get_node("Nothing")

	# Move the audio players to the location of the speaker in the box.
	stream_player_channel.global_transform = speaker.global_transform
	stream_player_who.global_transform = speaker.global_transform
	stream_player_hammer_one.global_transform = speaker.global_transform
	stream_player_hammer_two.global_transform = speaker.global_transform
	stream_player_hammer_three.global_transform = speaker.global_transform
	stream_player_nothing.global_transform = speaker.global_transform

func _on_channel_ball_eaten():
	stream_player_channel.play()

func _on_pete_ball_eaten():
	_score(10)
	stream_player_who.play()

func _on_bowling_ball_eaten():
	_score(5)
	stream_player_nothing.play()

func _on_tulip_one_ball_eaten():
	print("tulip1")
	_score(1)
	stream_player_hammer_one.play()

func _on_tulip_two_ball_eaten():
	print("tulip2")
	_score(1)
	stream_player_hammer_two.play()

func _on_tulip_three_ball_eaten():
	print("tulip3")
	_score(1)
	stream_player_hammer_three.play()
