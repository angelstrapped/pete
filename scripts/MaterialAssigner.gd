extends Spatial


export(Resource) var rail_physics_material
export(Array, NodePath) var rails


func _ready():
	for imported_object_path in rails:
		for mesh in get_node(imported_object_path).get_children():
			for node in mesh.get_children():
				if node.is_class("StaticBody"):
					node.set_physics_material_override(rail_physics_material)
