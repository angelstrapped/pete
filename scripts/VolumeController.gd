extends HSlider


export(String) var audio_bus_name

var audio_bus

func _ready():
	audio_bus = AudioServer.get_bus_index(audio_bus_name)
	value = AudioServer.get_bus_volume_db(audio_bus)

func _on_value_changed(value):
	AudioServer.set_bus_volume_db(audio_bus, value)
	print(AudioServer.get_bus_volume_db(audio_bus))
