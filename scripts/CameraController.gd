extends Camera


const LOOK_DISTANCE_FACTOR = 0.5

export(Array, NodePath) var default_position_paths

var default_positions = []
var current_position_index: int = 0

var current_x = 0.0
var current_y = 0.0

var x
var y


func _ready():
	for path in default_position_paths:
		default_positions.append(get_node(path))
	
	_reset_position()

func _process(_delta):
	if Input.is_action_just_pressed("camera_toggle"):
		_toggle_camera()
	
	x = Input.get_joy_axis(0, 0)
	y = Input.get_joy_axis(0, 1)
	if abs(x) < 0.3 and abs(y) < 0.3:
		x = 0.0
		y = 0.0

	# Debounce...? Whatever you would call this.
	if abs(current_x) - abs(x) > 0.05:
		x = 0.0
	if abs(current_y) - abs(y) > 0.05:
		y = 0.0

	current_x = lerp(current_x, x, 0.1)
	current_y = lerp(current_y, y, 0.1)
	
	global_transform.basis = default_positions[current_position_index].global_transform.basis.rotated(global_transform.basis.x, -current_y * LOOK_DISTANCE_FACTOR)
	transform.basis = transform.basis.rotated(transform.basis.y, -current_x * LOOK_DISTANCE_FACTOR)

func nudge(direction: Vector3):
	transform.origin += direction
	get_node("NudgeTimer").start()

func _on_NudgeTimer_timeout():
	_reset_position()

func _reset_position():
	transform.origin = default_positions[current_position_index].transform.origin

func _toggle_camera():
	current_position_index = (current_position_index + 1) % len(default_positions)

	_reset_position()
