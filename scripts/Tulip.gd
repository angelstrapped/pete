extends Spatial


signal ball_eaten

export(NodePath) var spring_left_path
export(NodePath) var spring_right_path

onready var spring_left: HingeJoint = get_node(spring_left_path)
onready var spring_right: HingeJoint = get_node(spring_right_path)


func _flip():
	# Reverse the tension direction of the springs.
	spring_left.set_param(HingeJoint.PARAM_MOTOR_TARGET_VELOCITY, -spring_left.get_param(HingeJoint.PARAM_MOTOR_TARGET_VELOCITY))
	spring_right.set_param(HingeJoint.PARAM_MOTOR_TARGET_VELOCITY, -spring_right.get_param(HingeJoint.PARAM_MOTOR_TARGET_VELOCITY))


func _on_ball_eaten():
	_flip()
	emit_signal("ball_eaten")
