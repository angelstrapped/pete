extends Spatial


const SLOWNESS = 6

export(NodePath) var next_reel_path
onready var next_reel = get_node(next_reel_path)
onready var stream_player = get_node("AudioStreamPlayer3D")


var reset_reel = false
var position: int = 0
var destination: int = 0
onready var frame: int = randi() % SLOWNESS


func add(number: int):
	destination += number

func reset():
	reset_reel = true
	
	if next_reel:
		next_reel.reset()

func _process(_delta):
	frame += 1
	if frame % SLOWNESS != 0:
		return
	frame = 0
	
	if reset_reel:
		if position % 10 != 0:
			_advance_reel()
		else:
			reset_reel = false
			position = 0
			destination = 0
	else:
		if destination > position:
			_count_up()

func _count_up():
	_advance_reel()
	if position % 10 == 0:
		if next_reel:
			next_reel.add(1)

func _advance_reel():
	set_rotation_degrees(get_rotation_degrees() + Vector3(36.0, 0.0, 0.0))
	position += 1
	stream_player.play()
