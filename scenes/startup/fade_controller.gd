extends ColorRect

var timer = -1.0
var alpha_factor
var direction

func _ready():
	#print("Fader exists.") # DEBUG725
	alpha_factor = 0.3

func _process(delta):
	if timer > 0.0:
		timer -= delta
		color -= Color(0, 0, 0, (delta / alpha_factor) * direction)

func fade_out(time: float):
	timer = time
	direction = 1

func fade_in(time: float):
	timer = time
	direction = -1
