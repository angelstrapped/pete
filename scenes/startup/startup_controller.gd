extends Control

export(Resource) var title_scene

# I've implemented the fading in a strange way which seems to ignore this value.
const FADE_TIME = 1.0
const SHOW_TIME = 1.0

onready var foreground = get_node("Foreground")
onready var logos = get_node("Logos")

func _ready():
	foreground.visible = true

	for logo in logos.get_children():
		logo.visible = false

	yield(get_tree().create_timer(1.0), "timeout")

	for logo in logos.get_children():
		logo.visible = true
		foreground.fade_out(FADE_TIME)
		yield(get_tree().create_timer(SHOW_TIME + FADE_TIME), "timeout")
		foreground.fade_in(FADE_TIME)
		yield(get_tree().create_timer(FADE_TIME), "timeout")
		logo.visible = false

	# For a more generic approach, simply change to the target scene and it will
	# be responsible for its own fade-in.
	foreground.fade_out(FADE_TIME)
	var _error = get_tree().change_scene_to(title_scene)

func _input(event):
	# If the player presses buttons during the title sequence, go straight to the main menu.
	# Not sure if there is a better or more generic way to define a set of
	# buttons to map to this same code path.
	if (
		event.is_action("nudge")
	):
		var _error = get_tree().change_scene_to(title_scene)
	
